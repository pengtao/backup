package cn.lifecycle.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.channels.FileChannel;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class Backup {
	private static File logfile = null;
	public static void main(String[] args) throws Exception {
		String JsonContext = ReadFile(args[0]+"\\conf.json");
		//日志文件
		logfile = new File(args[0]+"\\server.log");
		log("主程序开始！！！");
		JSONArray jsonArray = JSONArray.fromObject(JsonContext);
		if(null != jsonArray && jsonArray.size()>0){
			for (int i = 0; i < jsonArray.size(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				Config config = new Config();
				//最后修改时间modifiedDate
				Date modifiedDate = new Date();
				if(null != jsonObject.get("schema") && null != jsonObject.get("password")){
					//config实例化
					config = init(jsonObject);
					//config实例化完毕
					//查看table表是否在数据库存在
					String sql = "select count(*) from  user_tables t where t.TABLE_NAME ='"+config.getTable().toUpperCase()+"'";
					ResultSet rs = connect(config.getSchema(),config.getPassword(),config.getSid(),sql);
					if( rs!=null && rs.next()){
						sql = "select max(bfo.modifieddate) from "+ config.getTable() +" bfo";
						rs = connect(config.getSchema(),config.getPassword(),config.getSid(),sql);
						if(null != rs && rs.next()) {
							modifiedDate = rs.getTimestamp(1);
							if(null !=modifiedDate){
								backup(modifiedDate,config);
							}
						}
					}else{
						log("第"+i+"个json配置信息有误，可能原因：数据库用户名schema、密码password、表table或者数据库实例sid配置错误");
					}
					
				} else {
					log("第"+i+"个json配置，原因为：数据库用户名schema、密码password错误");
				}
			}
		}
		log("主程序备份结束！！！");
	}
	
	//写日志
	public static void log(String msg) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date nowTime = new Date();
		PrintWriter out = null;
		try {
			out = new PrintWriter(new FileWriter(logfile, true));
			out.write(sdf.format(nowTime)+":"+msg);
			out.write("\r\n");
		} catch (IOException e) {
			System.out.println("日志文件不存在");
		} finally {
			out.flush();
			out.close();
		}
	}
	
	//备份
	private static void backup(Date modifiedDate,Config config){
		/* 系统时间-modifiedDate<1小时，备份
		 * 如果是当前时间是12点或20点，并且在上一个20点或者12点之间有备份，备份。*/
		//系统时间
		Calendar sysdate = Calendar.getInstance();   // 当时的日期和时间
		int day = sysdate.get(Calendar.DAY_OF_MONTH);  // 取出“日”数
		--day;                                   // 将“日”减一，即得到前一天
		//上一天20点
		Calendar lastdate20 = Calendar.getInstance();
		lastdate20.set(Calendar.DAY_OF_MONTH, day);       
		lastdate20.set(Calendar.HOUR_OF_DAY, Integer.parseInt(config.getBackupTime2()));
		//今天的12点
		Calendar sysdate12 = Calendar.getInstance();
		sysdate12.set(Calendar.HOUR_OF_DAY, Integer.parseInt(config.getBackupTime1()));
		if(sysdate.getTime().getTime() - modifiedDate.getTime() <= 60*60*1000 || ((String.valueOf(sysdate.get(Calendar.HOUR_OF_DAY)) == config.getBackupTime1()) && (sysdate.getTime().getTime() - modifiedDate.getTime()) < (modifiedDate.getTime() - lastdate20.getTime().getTime())) || ((String.valueOf(sysdate.get(Calendar.HOUR_OF_DAY)) == config.getBackupTime2() && (sysdate.getTime().getTime() - modifiedDate.getTime()) < (modifiedDate.getTime() - sysdate12.getTime().getTime()))) ){
			//开始备份
			String _time = getTime();
			String command = "cmd.exe /c set nls_lang=SIMPLIFIED CHINESE_CHINA.ZHS16GBK && expdp "+config.getSchema()+"/"+config.getPassword()+"@"+config.getSid()+" SCHEMAS=" + config.getSchema() + " DUMPFILE="+config.getSchema()+ _time +".dmp DIRECTORY=USRDMP LOGFILE="+config.getSchema()+ _time +".log && copy " + config.getDirectory() +"\\"+config.getSchema()+ _time + ".dmp " + config.getRemote().replace("/", "\\");
			System.out.println(command);
			try {
				Runtime.getRuntime().exec(command);
			} catch (IOException e) {
				log("执行备份时出错");
			}
			//删除本地磁盘中的备份文件
			deleteFile(config);
			//删除远程盘备份文件
			copyFile(config);
			
		} else {
			log("前一小时内未修改，modifiedDate1或modifiedDate2距上一个时间点未作修改，未备份");
		}
	}
	
	/*获取所有备份文件*/
	public static List<File> getFiles(String path,String schema) {
		File file = new File(path);
		File[] files = file.listFiles();
		Pattern p = Pattern.compile("^"+schema+"[0-9]{12}((\\.dmp)|(\\.log))$");
		List<File> list = new ArrayList<File>();
		if(file.exists() && files != null && files.length > 1) {
			for(File dfile:files) {
				if(p.matcher(dfile.getName().toLowerCase()).matches()){
					list.add(dfile);
				}
			}
			
			Collections.sort(list, new Comparator<File>() {
				@Override
				public int compare(File o1, File o2) {
					Date o1Date = null;
					Date o2Date = null;
					SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
					if(o1.getName().length()>16 && o2.getName().length()>16) {
						try {
							o1Date = sdf.parse(o1.getName().substring(o1.getName().length()-16, o1.getName().length()-4));
							o2Date = sdf.parse(o2.getName().substring(o2.getName().length()-16, o2.getName().length()-4));
						} catch (ParseException e) {
							log("error:getFiles()时间解析语句异常");
						}
					}
					if( o1Date!=null && o2Date != null) {
						if(o1Date.getTime() < o2Date.getTime()) {
							return 1;
						} else if(o1Date.getTime() > o2Date.getTime()){
							return -1;
						}
					}
					return 0;
				}
			});
		}
		return list;
	}
	
	/*删除备份文件*/
	public static void deleteFile(Config config) {
		List<File> list = getFiles(config.getDirectory(),config.getSchema());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
		String lastFileDate = sdf.format(new Date());
		//最大时间yyyyMMddHHmm
		Date lastDate = new Date();
		//取消以文件中最大时间点为终点
//		try {
//			lastDate = sdf.parse(lastFileDate);
//		} catch (ParseException e) {
//			System.out.println("error:deleteFile方法时间转换异常");
//		}
		
		/*
		 * dmp备份文件名格式（固定）为 jeansfair201403280032.dmp
		 * 1. 如果这个文件的小时是 12:00 或则 20:00 return;
		 * 2. 如果这个文件的时间与系统时间在同一个小时的  return 修改为相隔在一个小时之内的return;
		 * 3. 如果当前文件的时间 - 系统时间 在8小时之内并且是整点的 return;
		 * 4. 如果当前文件时间与系统时间在相同一天，并且是整点return（已删除该约定）
		 */
		for(File file : list) {
			String otherfilename = file.getName();
			Date otherfileDate = null;
			try {
				if(otherfilename.length()>16) {
					otherfileDate = sdf.parse(otherfilename.substring(otherfilename.length()-16, otherfilename.length()-4));
				}
			} catch (ParseException e) {
				System.out.println("error:deleteFile方法时间转换异常");
			}
			/*
			 * 文件删除操作
			 */
			if(otherfilename.toLowerCase().matches(config.getSchema().toLowerCase()+".+"+config.getBackupTime1()+"00((\\.dmp)|(\\.log))$") || otherfilename.toLowerCase().matches(config.getSchema().toLowerCase()+".+"+config.getBackupTime2()+"00((\\.dmp)|(\\.log))$")) {
				continue;
			/*} else if(otherfilename.indexOf(lastFileDate.substring(0, lastFileDate.length()-2))!=-1) {
				continue;
			*/
			} else if(lastDate != null && otherfileDate != null && (lastDate.getTime() - otherfileDate.getTime() <= 1*60*60*1000)){
				continue;
			} else if(lastDate != null && otherfileDate != null && (lastDate.getTime() - otherfileDate.getTime() <= 8*60*60*1000) && otherfilename.toLowerCase().matches(config.getSchema().toLowerCase()+".+00((\\.dmp)|(\\.log))$")) {
				continue;
			/*} else if(lastDate != null && otherfileDate != null && (lastFileDate.substring(0, 8).equals(otherfilename.substring(otherfilename.length()-16, otherfilename.length()-8))) && otherfilename.toLowerCase().matches(c.getSchema()+".+00\\.dmp$")) {
				continue;*/
			} else {
				file.delete();
			}
		}
	}

	
	/*保存备份文件到远程盘*/
	public static void copyFile(Config config) {
		/* 远程备份盘 
		 * 1. 找到最新文件（时间） -8小时的所有文件。
		 * 2. 与远程对比，没有就copy，有就不进行操作。只有远程有的远程盘进行删除，本地不进行操作。
		 * */
		List<File> srcList = getFiles(config.getDirectory(),config.getSchema());
		File destFile = new File(config.getRemote());
		if(!destFile.exists() && !destFile.isDirectory()){
			destFile.mkdirs();
		}
		List<File> destList = getFiles(config.getRemote(),config.getSchema());
		if(srcList.size()>0) {
			List<File> temp = new ArrayList<File>();
			String newFileName = srcList.get(0).getName();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHH");
			Date newFileDate = new Date();
			//取消以文件中最大时间点为终点
//			try {
//				if(newFileName.length()>16) {
//					newFileDate = sdf.parse(newFileName.substring(newFileName.length()-16, newFileName.length()-6));
//				}
//			} catch (ParseException e1) {
//				System.out.println("error:copyFile方法文件名1解析异常");
//			}
			long befFileDate = 0;
			befFileDate = newFileDate.getTime()-8*60*60*1000;
//			if(newFileDate != null) {
//				befFileDate = newFileDate.getTime()-8*60*60*1000;
//				if(newFileName.length()>16 && (newFileName.substring(newFileName.length()-12, newFileName.length()-10)).equals("00")) {
//					befFileDate = newFileDate.getTime()-9*60*60*1000;
//				}
//			}
			for(File f:srcList) {
				String filename = f.getName();
				Date fileDate = null;
				try {
					if(newFileName.length()>16) {
						fileDate = sdf.parse(filename.substring(newFileName.length()-16, filename.length()-6));
					}
				} catch (ParseException e) {
					log("error:copyFile方法文件名1解析异常");
				}
				if(fileDate != null && newFileDate != null && fileDate.getTime()-befFileDate>0 && fileDate.getTime() - newFileDate.getTime() <= 0 && filename.toLowerCase().endsWith(".dmp")) {
					temp.add(f);
				}
			}
			
			/*遍历远程文件，如果远程文件与temp存放文件有重名，则temp删除需要备份的文件；没有则远程删除该备份文件*/
			if(destList.size()>0) {
				for(File dFile:destList) {
					boolean isRemove = true;
					for(File tFile:temp) {
						if(dFile.getName().equalsIgnoreCase(tFile.getName())) {
							temp.remove(tFile);
							isRemove = false;
							break;
						}
					}
					if(isRemove) {
						dFile.delete();
					}
				}
			}
			FileInputStream fi = null;
	        FileOutputStream fo = null;
	        FileChannel in = null;
	        FileChannel out = null;
			for(File tFile:temp) {
				try {
		            fi = new FileInputStream(tFile);
		            fo = new FileOutputStream(new File(config.getRemote()+"/"+tFile.getName()));
		            in = fi.getChannel();//得到对应的文件通道
		            out = fo.getChannel();//得到对应的文件通道
		            in.transferTo(0, in.size(), out);//连接两个通道，并且从in通道读取，然后写入out通道
		        } catch (IOException e) {
		        	log("error:copyFile方法获取文件通道异常");
		        } finally {
		            try {
		                fi.close();
		                in.close();
		                fo.close();
		                out.close();
		            } catch (IOException e) {
		            	log("error:copyFile方法流关闭异常");
		            }
		        }
			}
		}
	}
	//连接数据库
	private static ResultSet connect(String user,String pwd,String sid, String sql){
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:"+sid, user, pwd);
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
		} catch(Exception e){
			log("error:数据库连接失败");
		}
		return rs;
	}
	
	//时间格式化
	public static String getTime() {
		String _time = null;
		Date d = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("mm");
		SimpleDateFormat pre = new SimpleDateFormat("yyyyMMddHH");
		if(Integer.parseInt(sdf.format(d))<15) {
			_time = pre.format(d)+"00";
		} else if(Integer.parseInt(sdf.format(d))>14 && Integer.parseInt(sdf.format(d))<30) {
			_time = pre.format(d)+"15";
		} else if(Integer.parseInt(sdf.format(d))>29 && Integer.parseInt(sdf.format(d))<45) {
			_time = pre.format(d)+"30";
		} else if(Integer.parseInt(sdf.format(d))>44) {
			_time = pre.format(d)+"45";
		}
		return _time;
	}
	
	//实例化config
	private static Config init(JSONObject jsonObject){
		Config config = new Config();
		config.setSchema(jsonObject.get("schema").toString());
		config.setPassword(jsonObject.get("password").toString());
		if(null != jsonObject.get("remote")){
			config.setRemote(jsonObject.get("remote").toString());
		}
		if(null != jsonObject.get("directory")){
			config.setDirectory(jsonObject.get("directory").toString());
		}
		if(null != jsonObject.get("sid")){
			config.setSid(jsonObject.get("sid").toString());
			}
		Pattern p = Pattern.compile("^(([0-1][0-9])|2[0-3])$");
		if(null != jsonObject.get("backupTime1") && p.matcher(jsonObject.get("backupTime1").toString()).matches()){
			config.setBackupTime1(jsonObject.get("backupTime1").toString());
		}
		if(null != jsonObject.get("backupTime2") && p.matcher(jsonObject.get("backupTime2").toString()).matches()){
			config.setBackupTime2(jsonObject.get("backupTime2").toString());
		}
		if(null != jsonObject.get("table")){config.setTable(jsonObject.get("table").toString());}
		//让backupTime1始终大于backupTime2，方便后续流程开发
		if(Integer.parseInt(config.getBackupTime1()) > Integer.parseInt(config.getBackupTime2())){
			String temp = "";
			temp = config.getBackupTime1();
			config.setBackupTime1(config.getBackupTime2());
			config.setBackupTime2(temp);
		}
		return config;
	}
	
	private static String ReadFile(String Path) {
		BufferedReader reader = null;
		String laststr = "";
		try {
			FileInputStream fileInputStream = new FileInputStream(Path);
			InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, "UTF-8");
			reader = new BufferedReader(inputStreamReader);
			String tempString = null;
			while ((tempString = reader.readLine()) != null) {
				laststr += tempString;
			}
			reader.close();
		} catch (IOException e) {
			log("读取conf.json文件失败");
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					log("流关闭失败");
				}
			}
		}
		return laststr;
	}
}
//实体类
class Config {
	private String schema;
	private String password;
	private String directory = "D:\\USRDMP";
	private String sid = "ORCL";
	private String remote="D:\\USRDMP";
	private String backupTime1 = "12";
	private String backupTime2 = "20";
	private String table = "b_foitem";
	public String getSchema() {
		return schema;
	}
	public void setSchema(String schema) {
		this.schema = schema;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getDirectory() {
		return directory;
	}
	public void setDirectory(String directory) {
		this.directory = directory;
	}
	public String getSid() {
		return sid;
	}
	public void setSid(String sid) {
		this.sid = sid;
	}
	public String getRemote() {
		return remote;
	}
	public void setRemote(String remote) {
		this.remote = remote;
	}
	public String getBackupTime1() {
		return backupTime1;
	}
	public void setBackupTime1(String backupTime1) {
		this.backupTime1 = backupTime1;
	}
	public String getBackupTime2() {
		return backupTime2;
	}
	public void setBackupTime2(String backupTime2) {
		this.backupTime2 = backupTime2;
	}
	public String getTable() {
		return table;
	}
	public void setTable(String table) {
		this.table = table;
	}
	
}
